package sec_5_singleton;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * Suppose we want to test the ApplicationClass.totalPopulationOf() method.
 */
public class SingletonTest {

    /**
     * Note that to test, WE NEED TO CONNECT TO ACTUAL DATABASE.
     * Also note that we are asserting that population is static, and that live db WILL ALWAYS have Del = 11000.
     * Hence, the problem here is that we are:
     *      1. Connecting to a live database.
     *      2. Assuming values are static in that.
     *
     * Thus, as we can see, the problem with singleton is the private behaviour that it brings along with it.
     * That is, once we create a singleton, we don't have access to it's inner functioning.
     * That is, if this method could somehow mock the database, it would.
     * But since that db in initiated as a part of the singleton initiation (which we cannot change), hence we have to do it this way only.
     */
    @Test
    public void testTotalPopulationOfMethod() {
        ApplicationClass applicationClass = new ApplicationClass();
        final int popOfDel = applicationClass.totalPopulationOf("del");
        Assert.assertEquals(popOfDel, 11000);
    }

    /**
     * We are using a mock implementation of that singleton by injecting another mock class for that singleton db loader.
     */
    @Test
    public void testBetterTotalPopulationOfMethod() {
        BetterApplicationClass betterApplicationClass = new BetterApplicationClass(new MockDb());
        final int del = betterApplicationClass.getTotalPopulationOf("del");
        Assert.assertEquals(del, 11000);
    }
}

class MockDb implements PopulationExplorer {

    Map<String, Integer> map = new HashMap<>();
    MockDb() {
        map.putIfAbsent("del", 11000);
        map.putIfAbsent("nyc", 17000);
        map.putIfAbsent("tel", 5000);
    }

    @Override
    public int getTotalPopulationOf(String city) {
        return map.getOrDefault(city, 0);
    }
}