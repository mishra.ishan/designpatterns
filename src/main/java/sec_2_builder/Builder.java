package sec_2_builder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * We will create a builder ourself. Remember that the main motivation is to do piecewise construction of object instead of w wholesale construction.
 *
 * Use Case: Create an HTML builder. It will take the tag name and the text inside and it will build that html.
 *
 * For example if our previous case (ExistingBuilderInJava), we had 'ul' as the html element had a text inside it.
 * Also note that one html element may have multiple html child elements inside it. We need to make sure of that.
 *
 * So, we have to give a structure for creating html elements such that a proper html can be rendered.
 *
 * Also we will make the builder as a fluent interface. That is, we want that the builder to return the object build till now.
 *
 * For example, stringBuilder.append() method returns a stringBuilder instance only such that we can chain the appends together.
 * Like, sb.append("foo").append(" ").append("bar") = "foo bar"
 */
public class Builder {
    public static void main(String[] args) {
        HtmlBuilder builder = new HtmlBuilder("ul");
        builder.addChild("li", "hello").addChild("li", "world");
        System.out.println(builder.toString());
    }
}

class HtmlElement {
    String tag, text;
    List<HtmlElement> childElements = new ArrayList<>();
    private int indentSize = 4;

    public HtmlElement() {
    }

    public HtmlElement(String childTag, String childText) {
        this.tag = childTag;
        this.text = childText;
    }

    private String toStringImpl (int indentLevel) {
        StringBuilder sb = new StringBuilder();
        String newLine = System.lineSeparator();
        String spaces = String.join("", Collections.nCopies(indentLevel, " "));
        sb.append(String.format("%s<%s>%s", spaces, tag, newLine));
        String elementSpaces = String.join("", Collections.nCopies(indentLevel + (indentSize/2), " "));
        if(text != null && !text.equalsIgnoreCase("")) {
            sb.append(String.format("%s%s%s", elementSpaces, text, newLine));
        }
        for(HtmlElement child : childElements) {
            sb.append(child.toStringImpl(indentLevel + indentSize));
        }
        sb.append(String.format("%s<\\%s>%s", spaces, tag, newLine));
        return sb.toString();
    }

    @Override
    public String toString() {
        return toStringImpl(0); //indent level of the current element being traversed.
    }
}

class HtmlBuilder {
    String rootName;
    HtmlElement rootElement;

    public HtmlBuilder(String rootName) {
        this.rootName = rootName;
        rootElement = new HtmlElement();
        rootElement.tag = rootName;
    }

    public HtmlBuilder addChild (String childTag, String childText) {
        final HtmlElement htmlElement = new HtmlElement(childTag, childText);
        this.rootElement.childElements.add(htmlElement);
        return this;    //for fluent interface, just return an instance of itself
    }

    @Override
    public String toString() {
        return rootElement.toString();
    }
}
