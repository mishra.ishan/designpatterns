package sec_2_builder;

/**
 * Sometimes the object that we are to build is so complicated that a single builder cannot work. We need multiple builders to work in tandem.
 *
 */
public class FacetedBuilder {
    public static void main(String[] args) {

        /**
         * ---------------------------------------1.
         * Problem if not used inheritance in builders. PersonAddressBuilder is not a type of PersonFacetedBuilder. Hence, we cannot use PersonAddressBuilder to build up a person.
         *
         * Also, if we use inheritance, then PersonAddressBuilder can switch to PersonEmploymentBuilder anytime to build up employment information.
         */
//        new PersonFacetedBuilder()
//                .lives()
//                    .city("Delhi")
//                    .postCode("10092")
//                    .street("Swasthya Vihar");

        final PersonFaceted person = new PersonFacetedBuilder()
                .lives()
                    .city("Delhi")
                    .postCode("110092")
                    .street("Swasthya Vihar")
                .worksAt()
                /** worksAt() method Was only possible since street() method above returns a PersonAddressBuilder which is a type of
         PersonFacetedBuilder. AND PersonFacetedBuilder has worksAt method **/
                    .company("ixigo")
                    .position("Sr. Data Engineer")
                    .annualIncome(190000)
                /**
                 * Note that above, we are calling build of PersonEmployeeBuilder().        -----------------------------------------------------2.
                 * Thus, the final PersonFaceted, that is created, it is one, that is present for PersonEmployeeBuilder()
                 * IT IS NOT THE ORIGINAL PersonFaceted() WHICH WAS BUILT WHEN WE CALLED new PersonBuilder()
                 */
                .build();

        System.out.println(person);
    }
}

/**
 * Suppose we need to build up this person. Now this person have address part as well as employment part.
 * Ask in interview: Create two fluent builders which can do so.
 * Like PersonFaceted
 * .Builder
 * .LivesAt()
 * .streetAddress
 * .postCode
 * .city
 * .worksAt()
 * .companyName
 * .position
 * .annualIncome
 * .build()
 * <p>
 * This will be achieved by combining concepts of builder and facade pattern.
 */
class PersonFaceted {


    /**
     * Test: How many persons shall be created? Ans: 3.
     *      1. When we call new PersonBuilder().
     *      2. When we call lives()
     *          -> lives() will call new PersonAddressBuilder().
     *          -> new PersonAddressBuilder() will call super()
     *          -> super() (PersonBuilder constructor) will create new PersonFaceted().
     *      3. When we call worksAt() (same logic as 2).
     */
    public PersonFaceted() {
        System.out.println("PERSON CREATED");
    }

    //address
    String streetAddress, postcode, city;

    //employment
    String companyName, position;
    int annualIncome;

    @Override
    public String toString() {
        return "PersonFaceted{" +
                "streetAddress='" + streetAddress + '\'' +
                ", postcode='" + postcode + '\'' +
                ", city='" + city + '\'' +
                ", companyName='" + companyName + '\'' +
                ", position='" + position + '\'' +
                ", annualIncome=" + annualIncome +
                '}';
    }
}

/**
 * This builder needs to have 2 builders, one building address and another building employment information
 */
class PersonFacetedBuilder {
    protected PersonFaceted person = new PersonFaceted();

    public PersonFacetedBuilder() {
        System.out.println("--------------------> PARENT CONSTRUCTOR INVOKED. New person created " + person.hashCode());
    }

    public PersonAddressBuilder lives() {
        /** Will pass the person which exists right now
         *  But as explained in -------------------------------------------------2., don't we need the new PersonFaceted()? Because build shall be called by that only, right?
         *  Thus, we
         * */
        System.out.println("lives() --------> Passing Person " + person.hashCode());
        return new PersonAddressBuilder(person);


        /** If we use only the empty constructor for both, then the newly created person will only initialise the last builders fields.
         * In this case, since worksAt() is called last, then only job details shall be init) */
//        return new PersonAddressBuilder();
    }

    public PersonEmploymentBuilder worksAt() {
        System.out.println("worksAt() -------> Passing Person " + person.hashCode());
        return new PersonEmploymentBuilder(person);


        /** If we use only the empty constructor for both, then the newly created person will only initialise the last builders fields.
         * In this case, since worksAt() is called last, then only job details shall be init) */
//        return new PersonEmploymentBuilder();
    }

    public PersonFaceted build() {
        /**
         * Note that how the original person (the one called by new PersonBuilder() is the one being built)
         */
        System.out.println("Building Person: " + person.hashCode());
        return person;
    }
}

/**
 * This builder builds up person's address. But what kind of object is it building up?
 * It is building a person only in the end.
 * <p>
 * Thus, this builder does not have any existence on its own and it depends on PersonFacetedBuilder and it builds up a Person.
 * <p>
 * Thus, it is only better to get the person being built for it's construction, right?
 *
 * Does it need inheritance? Yes.
 *
 */
class PersonAddressBuilder extends PersonFacetedBuilder {
    protected PersonFaceted p;
    /**
     * If we try to set fields for p, it won't set fields for super.person (the object that we are building).
     * Why? (We are not building p)
     * Because when we call lives(), a new PersonFaceted() is created (via call to super(), as explained in inheritance tester class).
     * Now, if in construction we do something like this:-
     *
     * class PersonAddressBuilder extends PersonBuilder {
     *     PersonFaceted p;
     *     public PersonAddressBuilder(PersonFaceted inheritedPersonFromSuper) {
     *         this.p = inheritedPersonFromSuper;
     *     }
     * }
     *
     * Then the compiler (post compilation), will do the following:-
     *
     * class PersonAddressBuilder extends PersonBuilder {
     *      PersonFaceted p;
     *      public PersonAddressBuilder(PersonFaceted inheritedPersonFromSuper) {
     *          super();            <--------------------- added by compiler
     *          this.p = inheritedPersonFromSuper;
     *      }
     * }
     *
     * Thus, constructor of PersonBuilder shall be called which will create a new PersonFaceted and give it the name as this.person.
     *
     * Thus, on doing "this.p = inheritedPersonFromSuper", we are assigning the newly created object () to the Original PersonFaceted (Created from new PersonBuilder()).
     * But, we need to somehow discard the newly created object and continue with our original object.
     * That means, on calling build(), since we build person (created by last called Constructor), we need to point that person to the original person.
     * This could be done if during construction of our PersonAddressBuilder, we assign the superclass person (which is right now corresponding to super() call) to the original person.
     *
     * That is EXACTLY what we do.
     */

    public PersonAddressBuilder(){

    }

    public PersonAddressBuilder(PersonFaceted p) {
        /**
         * This won't work
         */
//        this.p = p;

        this.person = p;
    }

    public PersonAddressBuilder street(String s) {
        /**
         * Won't work
         */
//        p.streetAddress = s;

        person.streetAddress = s;
        return this;
    }

    public PersonAddressBuilder postCode(String postCode) {
        /**
         * Won't work
         */
//        p.postcode = postCode;

        person.postcode = postCode;
        return this;
    }

    public PersonAddressBuilder city(String city) {
        /**
         * Won't work
         */
//        p.city = city;

        person.city = city;
        return this;
    }
}

class PersonEmploymentBuilder extends PersonFacetedBuilder {
    protected PersonFaceted p;

    public PersonEmploymentBuilder() {
    }

    public PersonEmploymentBuilder(PersonFaceted p) {
        /**
         * Won't work (Similarly as for PersonEmploymentBuilder)
         */
//        this.p = p;

        this.person = p;
    }

    public PersonEmploymentBuilder company(String company) {
        person.companyName = company;
        return this;
    }

    public PersonEmploymentBuilder position(String position) {
        person.position = position;
        return this;
    }

    public PersonEmploymentBuilder annualIncome(int annualIncome) {
        person.annualIncome = annualIncome;
        return this;
    }
}