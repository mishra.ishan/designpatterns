package sec_2_builder;

/**
 *  Good question to ask in an interview to test someone's knowledge of generics and builders and fluidic interface:-
 *  Design an organizational flow with builders. We would have:-
 *      1. A person.
 *      2. An Employee which extends that person.
 *      3. A person builder, which would build a person
 *      4. An employee builder, which would build the employee.
 *
 *  The catch is that employee builder must be able to build up the fields of person too. For example, if we have a person name, then employee builder must have a method to build up the name.
 *
 * Check this link: https://stackoverflow.com/questions/26304527/recursive-generic-and-fluent-interface
 * Also check: https://stackoverflow.com/questions/7354740/is-there-a-way-to-refer-to-the-current-type-with-a-type-variable/7354847#7354847
 */
public class RecursiveGenericsFluentBuilder {
    public static void main(String[] args) {
        System.out.println(new EmployeeBuilder_1()
                .worksAt("Ixigo")
                .withName("Ishan")
                .build());  //Problem. There is no person being built from employee builder. Thus, name would be null. We need to build up a person from employee builder.

        System.out.println(new EmployeeBuilder_21()
        .worksAt("Ixigo")
        .withName("Ishan")
        .build());  //Still the same problem as above, name is null.

        System.out.println(new ProfessorBuilder_2_1_1()
        .teachesAt("CalTech")
        .withName("Andy")   //Will Give Class Cast Exception.
        .build());

//        System.out.println(new EmployeeBuilder_3()
//        .worksAt("Ixigo")
//        .withName("Ishan")
//        .build());
    }
}

class Person {
    String name;

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                '}';
    }
}

class Employee extends Person {
    String worksAt;

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", worksAt='" + worksAt + '\'' +
                '}';
    }
}

class Professor extends Person {
    String teachesAt;

    @Override
    public String toString() {
        return "Professor{" +
                "name='" + name + '\'' +
                ", teachesAt='" + teachesAt + '\'' +
                '}';
    }
}

class PersonBuilder_1 {
    protected Person p = new Person();

    public PersonBuilder_1 withName(String name) {
        p.name = name;
        return this;
    }

    public Person build() {
        return p;
    }
}

class EmployeeBuilder_1 extends PersonBuilder_1 {
    protected Employee e = new Employee();

    public EmployeeBuilder_1 worksAt(String jobName) {
        e.worksAt = jobName;
        return this;
    }

    //Could be done this. But WHAT IF there are like 20/30 fields in person. Then will we do them all?
    // We actually haven't used the fact that EmployeeBuilder_1 is extending PersonBuilder.
    // WE WANT TO USE WITHNAME() METHOD FROM PERSONBUILDER. Lets change that.
//    public EmployeeBuilder_1 withName (String name) {
//        e.name = name;
//        return this;
//    }

    public Employee build() {
        return e;
    }
}

/**
 * we need some way to call withName() method from employeeBuilder and make sure not to break the fluent interface.
 * withName():- must return an employee builder if called from employee builder.
 * withName():- must return a person builder if called from person builder.
 *
 * How to do that?
 * Basically we need to provide some type of context to withName() that what kind of builder has invoked it.
 * So, PERSONBUILDER NEED TO HAVE SOME SORT OF IDEA AS TO WHAT IS EXTENDING IT. IF IT HAS, THEN withName() METHOD CAN RETURN THE GUY WHO IS EXTENDING PERSONBUILDER.
 *
 * We can use generics here to circumvent the problem with employee builder 1.
 *
 */

/**
 * "T extends PersonBuilder_2" : Whatever is T, needs to have inhertited PersonBuilder_2.                           -------------------------------------1. See below
 */
class PersonBuilder_2_1<T extends PersonBuilder_2_1> {  //T would be extending Person builder, hence we can return T for withName()
    Person p = new Person();

    public T withName(String name) {
        p.name = name;  //PROBLEM: IF WE HAVE EMPLOYEE, THEN THIS WOULD ONLY GIVE NAME OF PERSON AND NOT EMPLOYEE.
//        return T ;   //PROBLEM: CANNOT RETURN A GENERIC SIGNATURE.
        /**
         * To solve above problem, we know that whatever is calling withName() is a type of PersonBuilder only. Hence we can cast Person Builder to "T" (T is the one calling withName())
         */
        return (T) this;
    }

    public Person build() {
        return p;
    }
}

/**
 * "T extends Person_builder_2<T>": Will give compile time error if anything else (other than the inheritor) is given.  ------------------------------------------------2. See below
 * It puts a STRICT check on what is extending PersonBuilder.
 * It says, whatever is extending person builder:-
 *  1. needs to extend person builder
 *  2. AND INFORM PERSON BUILDER OF HIS TYPE.
 *
 *  Tells exactly what kind of PersonBuilder is being extended. Thus, PersonBuilder<EmployeeBuilder> would mean withName() is to be provided for an employee builder and PersonBuilder<ProfessorBuilder> would mean withName() is to be provided for a professor builder.
 *
 *  Moreover, T tells us that
 *  class EmployeeBuilder extends PersonBuilder<EmployeeBuilder> {
 *      //So, in PersonBuilder class, withName method can return T.
 *      //Isn't it cool? We can use recursive generics so that superclass can use whoever is extending it in it's methods :).
 *  }
 */
class PersonBuilder_2_2<T extends PersonBuilder_2_2<T>> {
    Person p = new Person();

    public T withName(String name) {
        p.name = name;  //PROBLEM: IF WE HAVE EMPLOYEE, THEN THIS WOULD ONLY GIVE NAME OF PERSON AND NOT EMPLOYEE.
        return selfCast();    //We can replace this with a method call. Advantage? We can override the behaviour in ALL INHERITING CLASSES.
    }

    protected T selfCast() {
        return (T)this;
    }

    public Person build() {
        return p;
    }
}

class EmployeeBuilder_21 extends PersonBuilder_2_1<EmployeeBuilder_21> {
    Employee e = new Employee();

    public EmployeeBuilder_21 worksAt (String job) {
        e.worksAt = job;
        return this;
    }

    public Employee build() {
        return e;
    }
}

/**
 *    --------------------------------------------1.
 *    This is valid to code as per ----------------------------------1. But it is wrong. It will give Class
 */

class ProfessorBuilder_2_1_1 extends PersonBuilder_2_1<EmployeeBuilder_21> {   // Cast Exception at runtime.
    Professor professor = new Professor();

    public ProfessorBuilder_2_1_1 teachesAt (String job) {
        professor.teachesAt = job;
        return this;
    }

    public Professor build() {
        return professor;
    }
}

/**
 * ----------------------------------------------2.
 */
//class ProfessorBuilder_2_2_2 extends PersonBuilder_2_2<EmployeeBuilder_21> {    /** This won't even compile. Thus, it is a good approach. It gives error that ProfessorBuilder is extending another type of PersonBuilder (EmployeeBuilder). Thus, PersonBuilder would never know the true inheritor of it.

//}


/**
 * The problem in above approach is that when called employee builder withName(), withName is still trying to build up a person only.
 * PERSON BUILDER NEED TO HAVE AN IDEA AS TO WHO IS EXTENDING IT AND WHAT THEY ARE BUILDING. HENCE, TAKING A SECOND ARGUMENT FOR PERSON BUILDER.
 */
class PersonBuilder_3<S extends Person, T extends PersonBuilder_3> {
    protected S person;

    public T withName(String name) {
        person.name = name; //PROBLEM: WILL GIVE AN NPE IF CALLED FROM EMPLOYEE BUILDER OR EVEN IF CALLED FROM PERSON BUILDER.
        return (T) this;
    }

    public S build() {
        return person;
    }
}

class EmployeeBuilder_3 extends PersonBuilder_3<Employee, EmployeeBuilder_3> {
    protected Employee e = new Employee();

    public EmployeeBuilder_3 worksAt (String job) {
        e.worksAt = job;
        return this;
    }

    public Employee build() {
        return e;
    }
}