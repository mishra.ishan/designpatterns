package sec_2_builder;

/**
 * In java, we have Stringbuilder as an example of an existing builder which does piecewise construction of a string.
 * The toString() method of a string builder does the final building of the string.
 * Note that stringbuilder is nothing but a builder for string.
 */
public class ExistingBuilderInJava {
    public static void main(String[] args) {
        //Use case: This string is an html element. We need to make it so.
        String hello = "hello";
        System.out.println(String.format("<p>%s</p>\n", hello));  //easy if only 1 element.

        String[] words = {"hello", "world"};
        StringBuilder sb = new StringBuilder();
        sb.append("<ul>\n");
        for(String w : words) {
            sb.append(String.format("  <li>%s<\\li>\n", w));
        }
        sb.append("<\\ul>");

        System.out.println(sb.toString());
    }
}