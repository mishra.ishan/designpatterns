package sec_2_builder;

public class InheritanceTester {

    /**
     * On Running the example, note certain things:-
     *      1. First the super class CONSTRUCTOR IS CALLED. IT IS NOT EXACTLY BUILT.
     *              --> How? Compiler after compilation will place a "super()" call in the beginning of the constructor of subclass.
     *              --> Always Beginning only? Yes. call to super() SHOULD BE THE FIRST THING IN THE SUBCLASS CONSTRUCTOR. Else, won't compile.
     *              --> What if we have only param construction for superclass (dog)? Won't compile. Unless we pass --------------------------------------1.
     *      2. Super class constructor is called so that all necessary initializations can take place in super class (for fields being inherited) for building sub-class.
     *              --> Check Link: https://stackoverflow.com/questions/488727/inheritance-in-java-creating-an-object-of-the-subclass-invokes-also-the-constr
     *      3. TempJoin tj1 = new TempJoin() is always called, creating a new TempJoin (everytime constructor of Animal is invoked)
     *              --> Why? ALL OBJECTS WHICH ARE REQUIRED FOR CONSTRUCTION OF Animal SHALL BE INITIALISED.
     *              --> Since for initialization of an animal, a TempJoin() object IS TO BE CREATED, HENCE IT WILL BE CREATED.
     *
     * @param args
     */

    public static void main(String[] args) {
//        final Dog dog = new Dog();
        final Cat cat = new Cat();
        final GermanShephard germanShephard = new GermanShephard();
    }
}

class TempJoin {
    int a;

    public TempJoin() {
        System.out.println("TJ ---> Empty Constructor Called for Temp Join");
    }

    public TempJoin(int a) {    //DOES NOT TRIGGER EMPTY CONSTRUCTOR
        this.a = a;
        System.out.println("TJ -----> Param Constructor Called for Temp Join");
    }
}

class Animal {
    TempJoin tj1 = new TempJoin();  //TRIGGERS TempJoin EMPTY CONSTRUCTOR
    TempJoin tj2;
    TempJoin tj3 = new TempJoin(3); //TRIGGERS TempJoin PARAM CONSTRUCTOR
    public Animal() {
        System.out.println("Animal -------> Animal Created");
    }
}

class Dog extends Animal {
    /**
     * To Test ---------------1., uncomment the below method and comment the empty constructor for Dog().
     *  ------------------------------------1.
     */
//    public Dog(int a) {
//        System.out.println("Dog ---------> Dog Created");
//    }

    public Dog() {
    }
}

class GermanShephard extends Dog {
    public GermanShephard() {
        /** ------------------------------------1. */
//        super(10);
        System.out.println("GermanShephard -----------> German Shephard Created");
    }
}

class Cat extends Animal {
    public Cat() {
        System.out.println("Cat ---------> Cat Created");
    }
}