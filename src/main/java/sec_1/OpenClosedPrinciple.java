package sec_1;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * OCP + Specification Pattern (Specification pattern does not come under GOF design patterns. It is a pattern in enterprise engineering).
 *
 * It seems like this pattern is mainly associated with modules (methods and classes). By the way only methods and classes can be extended.
 *
 * OCP states that any code should be open for extension but closed for modification. So it is okay  for us to inherit things or implement interfaces, but not for modiciation.
 *
 * It is because your client might already have the code in the form of binary or something. Hence you don't want to modify that code which is already in production (that code is tested and verified). Hence any code (method, class) should be open for extension and closed for modification.
 */
public class OpenClosedPrinciple {
    public static void main(String[] args) {
        Product apple = new Product("Apple", Color.GREEN, Size.SMALL);
        Product tree = new Product("Tree", Color.GREEN, Size.LARGE);
        Product house = new Product("House", Color.BLUE, Size.LARGE);

        List<Product> products = Arrays.asList(apple, tree, house);

        ProductFilter pf = new ProductFilter();
        System.out.println("Green products (old):");
        pf.filterByColor(products, Color.GREEN)
                .forEach(p -> System.out.println(" - " + p.name + " is green"));

        // ^^ BEFORE

        // vv AFTER
        BetterFilter bf = new BetterFilter();
        System.out.println("Green products (new):");
        bf.filter(products, new ColorSpecification(Color.GREEN))
                .forEach(p -> System.out.println(" - " + p.name + " is green"));

        System.out.println("Large products:");
        bf.filter(products, new SizeSpecification(Size.LARGE))
                .forEach(p -> System.out.println(" - " + p.name + " is large"));

        System.out.println("Large blue items:");
        bf.filter(products,
                new AndSpecification<>(
                        new ColorSpecification(Color.BLUE),
                        new SizeSpecification(Size.LARGE)
                ))
                .forEach(p -> System.out.println(" - " + p.name + " is large and blue"));
    }
}

class ProductFilter
{
    public Stream<Product> filterByColor(List<Product> products, Color color)
    {
        return products.stream().filter(p -> p.color == color);
    }

    public Stream<Product> filterBySize(List<Product> products, Size size)
    {
        return products.stream().filter(p -> p.size == size);
    }

    public Stream<Product> filterBySizeAndColor(List<Product> products, Size size, Color color)
    {
        return products.stream().filter(p -> p.size == size && p.color == color);
    }
    // state space explosion. Currently we had only 2 criteria for filtering, but what if we had 3 criteria for filtering??
    // 3 criteria = 7 methods (all possible combination of filters)
}

// we introduce two new interfaces that are open for extension (but closed for modification).

/**
 * In specification pattern, we make an interface which has a single function which just states that a certain product SPECIFIES a certain CRITERIA. If we need filtering of any objects and we know that there could be ANY criterias for filtering, we can use SPECIFICATION PATTERN. Basically make a specification interface with just one method (which tells if the criteria is satisfied or not).
 */

/**
 * Main principle: "Tell don't ask". Let products themselves tell us if they satisfy the criteria or not.
 */
interface Specification<T>
{
    boolean isSatisfied(T item);
}

/**
 * We create all types of specification that we want to apply to a product. In this way, we can call any one product and this specification and the "isSatisfied()" method can return if that product satisfies the requirements or not. 1st specification, by colour.
 */
class ColorSpecification implements Specification<Product>
{
    private Color color;

    public ColorSpecification(Color color) {
        this.color = color;
    }

    @Override
    public boolean isSatisfied(Product p) {
        return p.color == color;
    }
}

/**
 * 2nd Specification: By size.
 */
class SizeSpecification implements Specification<Product>
{
    private Size size;

    public SizeSpecification(Size size) {
        this.size = size;
    }

    @Override
    public boolean isSatisfied(Product p) {
        return p.size == size;
    }
}

/**
 *3rd Specification: Combination of both. We can also take a list here in order to extend it better. It is called a "combinator".
 */
class AndSpecification<T> implements Specification<T>
{
    private Specification<T> first, second;

    public AndSpecification(Specification<T> first, Specification<T> second) {
        this.first = first;
        this.second = second;
    }

    @Override
    public boolean isSatisfied(T item) {
        return first.isSatisfied(item) && second.isSatisfied(item);
    }

}

/**
 * Now We have our specification. Given any 1 product / item and the specification that we want to apply to that product, we can safely say (without modifying any existing code) if the products meets the specification or not. But now, we need to do this for a list of items. We won't be given just 1 item. We would be given a list of items.
 *
 * Thus, we create filter interface for dealing with list of items. This is in line with ISP (Interface segregation principle). One interface is only responsible for 1 task. Thus, specification interface was only responsible for dealing with specification criteria for 1 item.
 *
 * Filter interface is responsible for filtering list of items and checking if they meet the provided specification or not.
 */
interface Filter<T>
{
    Stream<T> filter(List<T> items, Specification<T> spec);
}

/**
 * We just take list of items that we have to filter. Also the specification we want to apply to those items.
 */
class BetterFilter implements Filter<Product>
{
    @Override
    public Stream<Product> filter(List<Product> items, Specification<Product> spec) {
        return items.stream().filter(p -> spec.isSatisfied(p));
    }
}


enum Color
{
    RED, GREEN, BLUE
}

enum Size
{
    SMALL, MEDIUM, LARGE, YUGE
}

class Product
{
    public String name;
    public Color color;
    public Size size;

    public Product(String name, Color color, Size size) {
        this.name = name;
        this.color = color;
        this.size = size;
    }
}

