package sec_1;

// A. High-level modules should not depend on low-level modules.
// Both should depend on abstractions.

// B. Abstractions should not depend on details.
// Details should depend on abstractions.

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * By abstraction, we typically mean either an abstract class or an interface.
 *
 * So basically it means that we get a signature of something that we have to use, but we don't work with concrete implementation.
 *
 * Part A of Dependecy inversion principle is FAR MORE IMPORTANT than part B. That is, high level modules should not depend on low level module. Both should depend on abstractions.
 */
public class DependencyInversionPrinciple {
    public static void main(String[] args)
    {
        Person parent = new Person("John");
        Person child1 = new Person("Chris");
        Person child2 = new Person("Matt");

        // low-level module
        Relationships relationships = new Relationships();
        relationships.addParentAndChild(parent, child1);
        relationships.addParentAndChild(parent, child2);

        new Research(relationships);
    }
}

enum Relationship
{
    PARENT,
    CHILD,
    SIBLING
}

class Person
{
    public String name;
    // dob etc.


    public Person(String name) {
        this.name = name;
    }
}


/**
 * Relationship is a low level module.
 *
 * It is because it is related to internal storage and stuff.
 *
 * It just keeps a list and gives us access to that list.
 *
 * This modules single responsibility is to keep a list and provide us access to that list.
 */
class Relationships implements RelationshipBrowser
        /**
         * Note how LOW LEVEL MODULE IMPLEMENTS THAT abstraction. (HIGH LEVEL MODULE DEPENDS ON THAT ABSTRACTION)
         *
         * What is the role of that abstraction? THAT ABSTRACTION PROVIDES DETAILS WHICH LOW LEVEL MODULE NEEDS TO EXPOSE.
         *
         * In this example, the low level module needs to expose some sort of way to return the records from whatever way we have stored (say we have used List (like in this example), or stored in a db or in a set or map etc).
         *
         * Advantage of doing so? This way, we can change the internal storage (from list to say map, or set or even to a db), and the client (the one using high level module), won't even know the difference.
         */
{
    public List<Person> findAllChildrenOf(String name) {

        return relations.stream()
                .filter(x -> Objects.equals(x.getValue0().name, name)
                        && x.getValue1() == Relationship.PARENT)
                .map(Triplet::getValue2)
                .collect(Collectors.toList());
    }

    // Triplet class requires javatuples
    private List<Triplet<Person, Relationship, Person>> relations =
            new ArrayList<>();

    public List<Triplet<Person, Relationship, Person>> getRelations() {
        return relations;
    }

    public void addParentAndChild(Person parent, Person child)
    {
        relations.add(new Triplet<>(parent, Relationship.PARENT, child));
        relations.add(new Triplet<>(child, Relationship.CHILD, parent));
    }
}

/**
 * Research is a high level module.
 *
 * It allows us to perform some sort of operations on those low level constructs and so it is kind of higher.
 *
 * The end user deals with high level modules.
 *
 * He would only care about the final research, and not on the internal storage.
 *
 * He only wants to know who all are "John's" children.
 */
class Research
{
    /**
     * This way of doing this is breaking Dependency Inversion Principle. High level modules should not depend upon low level modules.
     *
     * In this implementation, research (high level module) depends upon relationship (low level module) for construction.
     *
     * How to avoid this? Use part B: High and low level modules should depend upon abstractions.
     *
     * So, first introduce an abstraction (abstract class or interface). Basically just give a signature for high level module to use.
     *
     * Do not provider high level module with internal implementation.
     */
    public Research(Relationships relationships)
    {
        // high-level: find all of john's children
        List<Triplet<Person, Relationship, Person>> relations = relationships.getRelations();
        relations.stream()
                .filter(x -> x.getValue0().name.equals("John")
                        && x.getValue1() == Relationship.PARENT)
                .forEach(ch -> System.out.println("John has a child called " + ch.getValue2().name));
    }

    /**
     * This way we can change the internal implementation of storing the relationships, and the client won't even know the difference.
     */
    public Research(RelationshipBrowser browser)
    {
        List<Person> children = browser.findAllChildrenOf("John");
        for (Person child : children)
            System.out.println("John has a child called " + child.name);
    }
}

/**
 * The abstraction, that we were talking about above (in Research section).
 *
 * Both, high level module and low level module should depend upon this abstraction.
 *
 */
interface RelationshipBrowser
{
    List<Person> findAllChildrenOf(String name);
}


class Triplet<A,B,C> {
    A one;
    B two;
    C three;

    public Triplet(A one, B two, C three) {
        this.one = one;
        this.two = two;
        this.three = three;
    }

    public A getValue0() {
        return one;
    }

    public B getValue1() {
        return two;
    }

    public C getValue2() {
        return three;
    }
}