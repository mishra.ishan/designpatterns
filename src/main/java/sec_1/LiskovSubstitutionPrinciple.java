package sec_1;

/**
 * We should be able to substitute a subclass (derived class) for a base class. If we have an API which takes a base class, then we should be able to substitue a subclass in place of that base class and things should not break.
 *
 * Violation of LSP results in incorrect code through inheritence. Thus this design principle is mainly associated with inheritence.
 *
 * We inherit a class and suddenly this class behaves in such a different way that we have broken the way in which the 'useIt' method (below) works.
 */
public class LiskovSubstitutionPrinciple {
    // maybe conform to ++. Rectangle (Base class) must be able to be substituted for a subclass (square) and things should work fine.
    static void useIt(Rectangle r)
    {
        int width = r.getWidth();
        r.setHeight(10);    //Very bad setter. It makes sense to have this setter in case of rectangle, but not for square.
        System.out.println("Expected area of " + (width*10) + ", got " + r.getArea());
    }

    public static void main(String[] args) {
        Rectangle rc = new Rectangle(2, 3);
        useIt(rc);

        Rectangle sq = new Square();
        sq.setHeight(5);
        useIt(sq);  //doesn't work here because we broke LSP. Basically the height (and width) of the square is now 5. In useIt method, we change the width of square to 10 and find area.

        sq.setWidth(10);
    }
}

//Base class
class Rectangle
{
    protected int width, height;

    public Rectangle() {
    }

    public Rectangle(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getArea() { return width*height; }

    @Override
    public String toString() {
        return "Rectangle{" +
                "width=" + width +
                ", height=" + height +
                '}';
    }

    public boolean isSquare()
    {
        return width == height;
    }
}

//Subclass
class Square extends Rectangle
{
    public Square() {
    }

    public Square(int size) {
        width = height = size;
    }

    @Override
    public void setWidth(int width) {
        super.setWidth(width);
        super.setHeight(width);
    }

    @Override
    public void setHeight(int height) {
        super.setHeight(height);
        super.setWidth(height);
    }
}

//Conforming to LSP. We don't need a base class and substituted class in this case. In short, we do not need a square class if it just has a special functionality as compared to rectangle without any new methods or fields. We can have both made instead with factory pattern. DISCARD SQUARE CLASS.
class RectangleFactory
{
    public static Rectangle newSquare(int side)
    {
        return new Rectangle(side, side);
    }

    public static Rectangle newRectangle(int width, int height)
    {
        return new Rectangle(width, height);
    }
}
