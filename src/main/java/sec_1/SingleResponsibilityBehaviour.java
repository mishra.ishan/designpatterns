package sec_1;

import java.io.File;
import java.io.PrintStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 *  One CLASS should handle only 1 responsibility. Remember that it is related to the whole class.
 */
public class SingleResponsibilityBehaviour {
    public static void main(String[] args) throws Exception {
        Journal j = new Journal();
        j.addEntry("I cried today");
        j.addEntry("I ate a bug");
        System.out.println(j);

        Persistence p = new Persistence();
        String filename = "/Users/ishanmishra/Desktop/notepad.txt";
        p.saveToFile(j, filename, true);

        // windows!
        Runtime.getRuntime().exec("notepad.exe " + filename);
    }
}
class Journal   //Single responsibility of journal is to allow some sort of access to journal and addition into journal.
{
    private final List<String> entries = new ArrayList<>();

    private static int count = 0;   //no of entries

    public void addEntry(String text) {
        entries.add("" + (++count) + ": " + text);
    }
    public void removeEntry(int index) {
        entries.remove(index);
    }
    @Override
    public String toString() {
        return String.join(System.lineSeparator(), entries);
    }

    // here we break SRP: Now journal also has persistence as a concern.
    public void save(String filename) throws Exception {
        try (PrintStream out = new PrintStream(filename))
        {
            out.println(toString());
        }
    }

    // here we break SRP
    public void load(String filename) {}
    // here we break SRP
    public void load(URL url) {}
}
//Adhering to SRP: Now if suppose tomorrow we want to change the mechanism to save a journal, we just need to change this persistence class. The whole business logic can be as it is.
class Persistence
{
    public void saveToFile(Journal journal,
                           String filename, boolean overwrite) throws Exception
    {
        if (overwrite || new File(filename).exists())
            try (PrintStream out = new PrintStream(filename)) {
                out.println(journal.toString());
            }
    }

    public void load(Journal journal, String filename) {}
    public void load(Journal journal, URL url) {}
}
