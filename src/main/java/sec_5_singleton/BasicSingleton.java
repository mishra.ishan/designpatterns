package sec_5_singleton;

public class BasicSingleton {
    public static void main(String[] args) {
        final SingleInstance singleton1 = SingleInstance.getSingleton();
        final SingleInstance singleton2 = SingleInstance.getSingleton();
        final SingleInstance singleton3 = SingleInstance.getSingleton();

        //true. All are pointing to same instance.
        System.out.println(singleton1 == singleton2 && singleton2 == singleton3);
    }
}

class SingleInstance {

    int val;
    static int count = 0;

    private SingleInstance() {
        count++;
    }

    public static final SingleInstance si = new SingleInstance();

    public static SingleInstance getSingleton() {
        return si;
    }

    public int getVal() {
        return val;
    }

    public int getCount() {
        return count;
    }

    public void setVal(int val) {
        this.val = val;
    }
}