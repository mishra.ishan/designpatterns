package sec_5_singleton;

import java.util.HashMap;
import java.util.Map;

/**
 * In software engineering, the multiton pattern is a design pattern which generalizes the singleton pattern.
 * Whereas the singleton allows only one instance of a class to be created, the multiton pattern allows for the controlled creation of multiple instances, which it manages through the use of a map.
 *
 * Note that it is not thread safe: https://stackoverflow.com/questions/1003026/hashmap-concurrency-issue/1003237#1003237
 * https://stackoverflow.com/questions/2688629/is-a-hashmap-thread-safe-for-different-keys?noredirect=1&lq=1
 *
 * To make it thread safe, use concurrent hash map instead of normal hashmap.
 * https://stackoverflow.com/questions/11126866/thread-safe-multitons-in-java
 *
 * In fact, whenever dealing with multithreaded environments, NEVER USE NORMAL HASHMAPS. Use concurrent hash maps.
 *
 * Note that another way to do this is by using enums: https://stackoverflow.com/questions/3164379/only-5-instances-of-a-class
 * But with enums, we would have a problem of saving state.
 */
public class Multiton {
    public static void main(String[] args) {
        final Printer primaryPrinter1 = Printer.getInstance(Subsystems.PRIMARY);
        final Printer primaryPrinter2 = Printer.getInstance(Subsystems.PRIMARY);
        final Printer secondaryPrinter = Printer.getInstance(Subsystems.SECONDARY);

        System.out.println(primaryPrinter1 == primaryPrinter2);
        System.out.println(secondaryPrinter == primaryPrinter1);
    }
}

/**
 * Suppose we have to create a printer and we only want 3 instances of the printer viz primary instance, secondary instance and auxiliary instance. Thus, we will need a map which will store the instances. One of the big advantages it has is lazy loading.
 */
enum Subsystems {
    PRIMARY,
    SECONDARY,
    AUXILIARY;
}

class Printer {
    private static Map<Subsystems, Printer> subsystemVsPrinterMap = new HashMap<>();
    private static int count = 0;
    private Printer() {
        count++;
        System.out.println("Creating Printer instance no: " + count);
    }

    /**
     * Lazy loading. Create instance only when required
     */
    public static Printer getInstance(Subsystems subsystems) {
        if(!subsystemVsPrinterMap.containsKey(subsystems)) {
            subsystemVsPrinterMap.put(subsystems, new Printer());
        }
        return subsystemVsPrinterMap.get(subsystems);
    }
}