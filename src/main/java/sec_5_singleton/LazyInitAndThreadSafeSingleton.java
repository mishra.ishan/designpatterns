package sec_5_singleton;

/**
 * What is lazy init:-
 *  -> Create a new instance of the singleton ONLY WHEN SOMEONE REQUIRES IT.
 *  -> That is, only create a new instance IF SOMEONE CALLS getInstance() method.
 *  -> Will save unnecessary space used by the singleton (which might not be used).
 */
public class LazyInitAndThreadSafeSingleton {
}

class CustomLazyInitThreadSafeSingleton {
    static int count = 0;
    private static CustomLazyInitThreadSafeSingleton singleton;

    /**
     * -> This method is not thread safe. 2 contending threads which reach 'if(singleton == null)' statement at same time might instantiate the singleton twice.
     * -> To create it threadsafe, we can make the entire method as synchronized.
     * -> But this will then be an obvious performance hinderance.
     * -> Instead we do what is called a "double checked locking" (as done in next method)
     */
//    public static /**synchronized*/ CustomLazyInitThreadSafeSingleton getInstance() {
//        if(singleton == null) {
//            singleton = new CustomLazyInitThreadSafeSingleton();
//        }
//        return singleton;
//    }

    /**
     * Double checked locking involves double checks (DUH, obviously as name suggests):-
     *   1. Write the predicate that tests the entry into critical section
     *   2. Write syncrhonized statement
     *   3. Write the predicate that tests the entry into critical section
     */
    public static CustomLazyInitThreadSafeSingleton getInstance() {
        if(singleton == null) { /**1. Write the predicate that tests the entry into critical section*/
            synchronized (CustomLazyInitThreadSafeSingleton.class) {    /**2. Write syncrhonized statement*/
                if(singleton == null) { /**3. Write the predicate that tests the entry into critical section*/
                    singleton = new CustomLazyInitThreadSafeSingleton();
                }
            }
        }
        return singleton;
    }
}