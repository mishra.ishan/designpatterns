package sec_5_singleton;

import java.io.File;
import java.io.IOException;

public class StaticBlockSingleton {
    public static void main(String[] args) {
        final CustomSingletonUsingStaticBlock instance1 = CustomSingletonUsingStaticBlock.getInstance();
        final CustomSingletonUsingStaticBlock instance2 = CustomSingletonUsingStaticBlock.getInstance();

        System.out.println(instance1 == instance2 && instance1.getCount() == instance2.getCount());
    }
}

class CustomSingletonUsingStaticBlock {
    /**
     * Note that now we are not able to create a new object like this
     */
//    private static CustomSingletonUsingStaticBlock singleton = new CustomSingletonUsingStaticBlock();

    private static CustomSingletonUsingStaticBlock singleton;

    /**
     * Thus, we HAVE TO USE static block initialization.
     */
    static {
        try {
            singleton = new CustomSingletonUsingStaticBlock();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static int count = 0;

    private CustomSingletonUsingStaticBlock() throws IOException {
        count++;
        // Some exception type of initialization. Suppose we WANT that whenever this class is initialized, we create a new file
        File.createTempFile( "File Number:" + count, "txt");
    }

    public static CustomSingletonUsingStaticBlock getInstance() {
        return singleton;
    }

    public int getCount() {
        return count;
    }
}