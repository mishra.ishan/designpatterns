package sec_5_singleton;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class SingletonAndTestingProblems {

    public static void main(String[] args) {
        ApplicationClass applicationClass = new ApplicationClass();
        System.out.println(applicationClass.totalPopulationOf("Del"));

        /**
         * For Production purposes, we will pass it with actual singleton.
         * For Testing purposes, we will pass it with a dummy (see the code in test)
         */
        BetterApplicationClass betterApplicationClass = new BetterApplicationClass(BetterSingletonDb.getInstance());
    }
}

class ApplicationClass {

    /**
     * Only required totalPopulationOf Method for application.
     */
    public int totalPopulationOf (String cityCode) {
        SingletonDBLoader instance = SingletonDBLoader.getInstance();
        if(instance.getCityToPopulationMapping().containsKey(cityCode)) {
            return instance.getCityToPopulationMapping().get(cityCode);
        }
        return 0;
    }
}

class SingletonDBLoader {
    private SingletonDBLoader() throws IOException {
        System.out.println("Init singleton");
        System.out.println("Connecting to DB");
        try(FileReader fr = new FileReader("db.txt");
        BufferedReader br = new BufferedReader(fr);) {
            String line = "";
            while ((line = br.readLine()) != null) {
                final String[] split = line.split(" ");
                cityToPopulationMapping.putIfAbsent(split[0], Integer.parseInt(split[1]));
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private Map<String, Integer> cityToPopulationMapping = new ConcurrentHashMap<>();
    private static SingletonDBLoader singleton;

    static {
        try {
            singleton = new SingletonDBLoader();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static synchronized SingletonDBLoader getInstance() {
        return singleton;
    }

    public Map<String, Integer> getCityToPopulationMapping() {
        return cityToPopulationMapping;
    }
}

/**
 * Problem in above approach, low level module (our singleton) is directly interacting with high level module.
 * As per dependency inversion principle, both should depend upon abstractions. Hence doing that.
 */

/**
 * Abstraction
 */
interface PopulationExplorer {
    int getTotalPopulationOf(String city);
}

/**
 * Low level module implementing that abstraction
 */
class BetterSingletonDb implements PopulationExplorer {

    private BetterSingletonDb() {
        System.out.println("Init singleton");
        System.out.println("Connecting to DB");
        try(FileReader fr = new FileReader("db.txt");
            BufferedReader br = new BufferedReader(fr);) {
            String line = "";
            while ((line = br.readLine()) != null) {
                final String[] split = line.split(" ");
                cityVsPopulationCountMap.putIfAbsent(split[0], Integer.parseInt(split[1]));
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private static final BetterSingletonDb instance = new BetterSingletonDb();
    private Map<String, Integer> cityVsPopulationCountMap = new ConcurrentHashMap<>();

    @Override
    public int getTotalPopulationOf(String city) {
        return cityVsPopulationCountMap.getOrDefault(city, 0);
    }

    public static BetterSingletonDb getInstance() {
        return instance;
    }
}

/**
 * High Level Module depending upon that abstraction instead of the depending upon that low level module
 */
class BetterApplicationClass {
    final PopulationExplorer populationExplorer;
    BetterApplicationClass(PopulationExplorer populationExplorer) {
        this.populationExplorer = populationExplorer;
    }

    public int getTotalPopulationOf(String city) {
        return populationExplorer.getTotalPopulationOf(city);
    }
}
