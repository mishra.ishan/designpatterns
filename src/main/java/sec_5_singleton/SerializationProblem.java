package sec_5_singleton;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 *
 * The basic singleton that we made earlier (BasicSingleton.java) can be broken easily by using
 * 1. Reflection (https://stackoverflow.com/questions/6911427/is-it-possible-to-invoke-private-attributes-or-methods-via-reflection)
 * 2. Serialization (https://www.youtube.com/watch?list=PLoKSeJT_PsoSohgFoNKU9jF3ZuNT4DsiW&v=pi_I7oD_uGI&feature=player_detailpage#t=1910s) (Specifically don't use readResolve() Method) (see 28th Min of this talk)
 *              -> https://stackoverflow.com/questions/3930181/how-to-deal-with-singleton-along-with-serialization :-> Explains why even enums are not break proof using serialization
 * 3. Cloning: Not that important. Since clone() of a class is protected by default and singleton does not have any public default constructor, so NO CLASS CAN EXTEND IT.
 * -> Thus, there won't be any subclass possible, and no one can call clone() of the superclass. (https://stackoverflow.com/questions/9313058/singleton-design-pattern-and-preventing-cloning)
 *
 *  Also check out these doc: http://staff.cc/java/jdk1.4.1/guide/serialization/spec/output.doc5.html#5324
 */
public class SerializationProblem {
    public static void main(String[] args) throws IllegalAccessException, InstantiationException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException {
        CustomSerializableSingleton instance = CustomSerializableSingleton.getInstance(5);

        //1. Break through reflection
//        Class<? extends CustomSerializableSingleton> aClass = (Class<CustomSerializableSingleton>) Class.forName("sec_5_singleton.CustomSerializableSingleton");
//        final Constructor<? extends CustomSerializableSingleton> declaredConstructor = aClass.getDeclaredConstructor();
//        declaredConstructor.setAccessible(true);
//        final CustomSerializableSingleton customSerializableSingleton1 = declaredConstructor.newInstance();
//        System.out.println("---------------------------------- Reflection ---------------------------------------");
//        System.out.println(instance == customSerializableSingleton1);
//        System.out.println(instance.getCount() + " -> " + instance.getVal());
//        System.out.println(customSerializableSingleton1.getCount() + " -> " + customSerializableSingleton1.getVal());

        //2. Break through serialization
        final String path = "customSerializableObject.bin";
        saveToFile(instance, path);
        instance.setVal(10);
        CustomSerializableSingleton customSerializableSingleton = readFromFile(path);

        System.out.println("----------------------------------- Serialization ------------------------------------");
        System.out.println(instance == customSerializableSingleton);
        System.out.println(instance.getCount() + " -> " + instance.getVal());
        System.out.println(customSerializableSingleton.getCount() + " -> " + customSerializableSingleton.getVal());
    }

    private static void saveToFile(CustomSerializableSingleton singleton, String path) {
        try (FileOutputStream oStream = new FileOutputStream(path);
             ObjectOutputStream ooStream = new ObjectOutputStream(oStream)) {
            ooStream.writeObject(singleton);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private static CustomSerializableSingleton readFromFile(String path) {
        try (FileInputStream fis = new FileInputStream(path);
             ObjectInputStream ois = new ObjectInputStream(fis)) {
            System.out.println("Reading from file.....");
            return (CustomSerializableSingleton) ois.readObject();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
}

class CustomSerializableSingleton implements Serializable {
    int val;
    static int count = 0;

    //Prevention for breaking via reflection: https://stackoverflow.com/questions/6994393/singleton-how-to-stop-create-instance-via-reflection

    /**
     *      ------------------------------------------------ TO STOP BREAKING VIA REFLECTION (uncomment the commented lines)--------------------------------------------------------
     * ->  Basically, how we break using reflection is that we ultimately set accessibility of this constructor as true, and then call the constructor (as showed in demo above).
     * -> So, what if, even after setting the constructor as true, we still do not allow making of multiple objects?
     * -> That is, what if someone calls this constructor, but if the object is created, we send an exception??
     */
    private CustomSerializableSingleton() {
        /** UNCOMMENT THESE LINES TO STOP THE ATTACK */
        System.out.println("CONSTRUCTOR CALLED.....");
//        if (CustomSerializableSingleton.INSTANCE != null) {
//            throw new InstantiationError("CANNOT INSTANTIATE A SINGLETON");
//        }
        count++;
    }

    public static final CustomSerializableSingleton INSTANCE = new CustomSerializableSingleton();

    public static CustomSerializableSingleton getInstance(int val) {
        INSTANCE.val = val;
        return INSTANCE;
    }

    public int getVal() {
        return val;
    }

    public void setVal(int val) {
        this.val = val;
    }

    public int getCount() {
        return count;
    }

    /**
     *
     * http://staff.cc/java/jdk1.4.1/guide/serialization/spec/input.doc7.html#5903
     * readResolve() method is called BEFORE RETURNING THE OBJECT USING readObject() METHOD IN ObjectInputStream.
     * -> Note that this method is just a placeholder, i.e., it is called AFTER THE DESERIALIZED OBJECT IS FULLY CONSTRUCTED.
     * -> After the deserialization and creation of a new instance, jvm checks if the class being deserialized and read implements readResolve.
     * -> If it does implement readResolve, then that method is called and whatever this (readResolve) method returns, is returned to the client.
     */
    protected Object readResolve() {
        return INSTANCE;
    }

}