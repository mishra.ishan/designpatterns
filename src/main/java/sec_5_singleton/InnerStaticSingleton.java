package sec_5_singleton;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * This is the Bill Pughs solution for creating lazy-loaded and thread-safe singletons.
 * The only problem with them is that they are prone to attacks by reflection.
 * If the singleton is heavy in construction (ie, requires a lot of memory), then it is better to use Bill Pughs solution instead of enums.
 *
 * https://stackoverflow.com/questions/6109896/singleton-pattern-bill-pughs-solution
 * https://dzone.com/articles/singleton-bill-pugh-solution-or-enum
 *
 * Mixture of enum way and Bill Pughs: https://stackoverflow.com/questions/12883051/enum-and-singletons-top-level-vs-nested-enum
 */
public class InnerStaticSingleton {
    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        final CustomInnerStaticSingleton instance = CustomInnerStaticSingleton.getInstance();

        //Break via reflection (same way as any singleton, just make the constructor public)
        final Class<CustomInnerStaticSingleton> singletonClass = (Class<CustomInnerStaticSingleton>) Class.forName("sec_5_singleton.CustomInnerStaticSingleton");
        final Constructor<CustomInnerStaticSingleton> declaredConstructor = singletonClass.getDeclaredConstructor(null);
        declaredConstructor.setAccessible(true);
        final CustomInnerStaticSingleton instance2 = declaredConstructor.newInstance();

        System.out.println(instance == instance2);
        System.out.println(instance.getCount() + " " + instance2.getCount());
    }
}

class CustomInnerStaticSingleton {
    static int count = 0;

    public int getCount() {
        return count;
    }

    private CustomInnerStaticSingleton() {
        count++;
    }

    /**
     * -> The inner static classes are only created if called.
     * -> getInstance() calls this class, hence this class shall only be created once getInstance() is called (ensuring lazy loading).
     * -> Thread Safe? Because:-
     *      -> Say 2 threads arrive at getInstance() concurrently.
     *      -> JVM will now start to build / load the inner class.
     *      -> While loading the class, it will stall these threads. (JLS ensures thread-safe class loading)
     *      -> Once built, both these threads can now only see the 1 loaded class.
     *      -> The two threads cannot force the inner static class to be loaded again.
     *
     * JLS: Java Language Specification
     */
    private static class InnerLazyLoadedClass {
        private static final CustomInnerStaticSingleton singleton = new CustomInnerStaticSingleton();
    }

    public static CustomInnerStaticSingleton getInstance() {
        return InnerLazyLoadedClass.singleton;
    }
}