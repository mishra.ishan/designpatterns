package sec_5_singleton;

import java.io.*;

/**
 * Enum based singleton are one of the most followed practices of creating enum. It cannot be even broken by reflection and is thread safe (as per JLS).
 * But, it still has its own shortcomings:-
 * -> Serialization Problem:-
 * Enums are serialized in a very peculiar fashion:
 * -> In cases of normal serialization, the constructors of serializable classes are never called. (https://stackoverflow.com/questions/8141440/how-are-constructors-called-during-serialization-and-deserialization)
 * -> Suppose there is any non serializable parent in the serialization hierarchy, then that parent IS SUPPOSED TO HAVE A DEFAULT NO-ARGS CONSTRUCTOR.
 * -> If not, then compiler will create one automatically and it will run without any problem. --------------------------------------------1.
 * -> No constructor is called for deserialized classes (they are made from deserialization).
 * -> Constructor is only called for first non-serializable deserialized class (Parent).
 * <p>
 * BUT IN CASES ON ENUM
 * -> Only Name of the Enum is serialized.
 * -> In our enum, Only the instance name (SINGLETON) shall be serialized.
 * -> It won't matter whatever we put the value for 'field', it won't be serialized.
 * -> Hence, in cases of enums, state is not persisted.
 * -> Also, when we shall deserialize the enum, then a constructor shall be called IF THE ENUM IS NOT ALREADY PRESENT IN THE MEMORY. ----------------------------------------2.
 */
public class EnumBasedSingleton {
    public static void main(String[] args) throws IOException {
        System.out.println("STARTING");
        EnumSingleton singleton1 = EnumSingleton.SINGLETON;
        EnumSingleton singleton2 = EnumSingleton.SINGLETON;
        /**
         * Note how both are same on memory.
         */
        singleton1.setField(10);
        System.out.println(singleton1 == singleton2);
        singleton2.setField(20);
        System.out.println(singleton1.getField() + " -> " + singleton2.getField());


        /**
         * ------------------------------------------------------1. (Need to uncomment)
         */
//        try (
//                ByteArrayOutputStream baos = new ByteArrayOutputStream();
//                ObjectOutputStream oos = new ObjectOutputStream(baos);
//        ) {
//            final Child child = new Child(5);
//            oos.writeObject(child);
//            try (
//                    ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
//                    ObjectInputStream ois = new ObjectInputStream(bais)
//            ) {
//                final Child child1 = (Child) ois.readObject();
//                System.out.println(child);
//                System.out.println(child1);
//            }
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        }

        String filename = "enumSerialized.bin";
        System.out.println("---------------------------------------- SERIALIZATION TEST ------------------------------------------");
        try (FileOutputStream fos = new FileOutputStream(filename);
             ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            EnumSingleton singleton3 = EnumSingleton.SINGLETON;
            oos.writeObject(singleton3);    /** NOTE THAT FIELD VALUE CURRENTLY IS 20 */
            try (FileInputStream fis = new FileInputStream(filename);
                 ObjectInputStream ois = new ObjectInputStream(fis)) {
                singleton3.setField(100);
                final EnumSingleton singleton4 = (EnumSingleton) ois.readObject();/** NOTE HOW FIELD WAS NOT SERIALIZED. HENCE WE ARE GETTING FIELD VALUE AS 100. ONLY NAME WAS SERIALIZED. */
                System.out.println(singleton3 == singleton4);
                System.out.println(singleton3.getField() + " " + singleton4.getField());
            }
        } catch (Exception ex) {

        }

        /**
         * But, if there was no instance of THAT ENUM VALUE present before, then constructor will be called. It can be checked by commenting EVERYTHING ABOVE and running just this below code
         */
//        String filename2 = "enumSerialized.bin";    /** This must be present (NOTE HOW WE SERIALIZED WITH VALUE 20 BEFORE) */
//        try(FileInputStream fis = new FileInputStream(filename2);
//        ObjectInputStream ois = new ObjectInputStream(fis)) {
//            final EnumSingleton singleton5 = (EnumSingleton) ois.readObject();
//            System.out.println(singleton5.getField());  /** CONSTRUCTOR SHALL BE CALLED */
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        }
    }
}

enum EnumSingleton {
    SINGLETON;  /** IF WE HAVE MORE THAN 1 ENUM CONSTANT, THEN WE CAN HAVE 2 INSTANCES OF THIS ENUM IN MEMORY (NO LONGER SINGLETON) */
    /**
     * Thus, number of enum values we have, that many instances are possible for that enum.
     * */

    private int field;

    EnumSingleton() {
        System.out.println("ENUMSINGLETON:::::CONSTRUCTOR");
        field = 10;
    }

    public int getField() {
        return field;
    }

    public void setField(int field) {
        this.field = field;
    }
}

class Parent {
    /**
     * Note how compiler will make one default constructor by itself.
     */
    protected int field;

    /**
     * Note how if we uncomment these lines, code won't compile....
     * First non-serializable class (Parent) in a serializable class hierarchy (Child) must have a default no-args constructor, or must provide room for compiler to do that.
     */
//    protected Parent (int field) {
//        this.field = field;
//    }
}

class Child extends Parent implements Serializable {
    int a;

    Child(int a) {
        System.out.println("Child::Constructor");
        this.a = a;
    }

    @Override
    public String toString() {
        return "Child{" +
                "field=" + field +
                ", a=" + a +
                '}';
    }
}