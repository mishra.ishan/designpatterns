package sec_5_singleton;

import java.util.function.Supplier;

class SingletonCodingExercise
{

    /**
     * Supplier<T> as the name suggests is a Supplier of type T.
     * This function has no argument.
     * It just has one method: 'get()'.
     * Whenever we call 'get()', we 'get()' a T type (maybe a new type, or maybe a singleton. Depends on the supplier).
     *
     * Thus, if a class implements Supplier<T>, then note that class will implement 'get()' method and inside that method, it will lay down how it will get you that underlying object.
     */
    public static boolean isSingleton(Supplier<Object> func) {
        final Object o1 = func.get();
        final Object o2 = func.get();
        return o1 == o2;
    }
}
