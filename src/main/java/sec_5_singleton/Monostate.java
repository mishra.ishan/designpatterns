package sec_5_singleton;

/**
 * Monostate pattern dictates that for creating a class as singleton, leave everything as it is, JUST CHANGE THE STORAGE OF THE CLASS TO STATIC.
 * Bad practice as there would still be multiple instances on heap. Hence, it will be confusing for user. Just that all will point to the same storage and hence would have same values.
 */
public class Monostate {
    public static void main(String[] args) {
        final CEO ceo1 = new CEO();
        ceo1.setName("Adam Smith");
        ceo1.setAge(55);

        final CEO ceo2 = new CEO();
        ceo2.setName("Dalia Wick");
        ceo2.setAge(62);
        System.out.println(ceo1 == ceo2);   /** NOTE HOW NOT SAME INSTANCES */
        /**
         * BUT BOTH HAVE SAME VALUES.
         */
        System.out.println(ceo1);
        System.out.println(ceo2);
    }
}

/**
 * To make it singleton, as per monostate, just change it's storage (name and age fields) to static.
 */
class CEO {
//    String name;
//    int age;
    static String name;
    static int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "CEO{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}