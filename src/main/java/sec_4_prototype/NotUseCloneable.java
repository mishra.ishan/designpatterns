package sec_4_prototype;

import java.util.Arrays;

/**
 * Why is cloneable croken?
 * Check out https://www.artima.com/intv/bloch.html#part13
 * There, check out "Copy Constructor versus Cloning"
 *
 * Basic Gist:
 * 1. Cloneable is a marker interface that does not have clone() method.
 *      -> Thus calling clone would essentially mean calling super.clone() (ie clone for Object class).
 *      -> Thus, if we have an array of cloneable objects, then we can't clone that array simply by going from object to object and calling clone on all of those.
 *      -> Why? Because calling clone on all of them might not be possible ('implements cloneable' does not expose clone() method)
 *      -> Moreover, the clone() method in Object class is protected. Hence, we would have to first make it public in order to be visible.
 * 2. Since clone() method of 'Object' class does field-by-field copy, generally the state of the object is also copied.
 *      -> Hence, if any object in the hierarchy was mutable, then the cloned object is also mutable, and changing one field of that clone would change the other too.  (see ------------------------------------------------1.)
 *      -> Also, constructor is not called when cloning using clone(). Thus, some construction points which the programmer intended might be missed.
 */
public class NotUseCloneable {
    public static void main(String[] args) throws CloneNotSupportedException {
        String[] iname = {"Ishan", "Mishra"};
        Address iaddress = new Address("Swasthya Vihar", 504, 1);
        Person ishan = new Person(iname, iaddress);
        System.out.println(ishan);

        /**
         * Now, suppose we need to create a person for aishwarya misra. Since her address shall be same, we do not need to create everything the same. If we could just take ishan person as a prototype and clone ishan, and then just change the name, that would be the best.
         */

        /**Person aishwarya = ishan.clone();   /**First problem: clone() is a protected member function. No problem, just override it and make it public*/
        Person aishwarya = (Person) ishan.clone();
        aishwarya.personName[0] = "Aishwarya";
        aishwarya.personAddress.residentNumber = 2;
        System.out.println(aishwarya);


        /**
         * -----------------------------------------------------1.
         * On running the below example, you will understand why not to use clone.
         * Reason:-
         * Clone1 has a state of Clone2 (which is mutable).
         * Hence, when we do field-by-field copy, then just THE REFERENCE TO CLONE2 IS COPIED.
         * Hence, we still have only 1 clone2 object on the heap.
         * Any changes done to clone2 results in changes in cloned object too.
         */
        Clone2 clone2 = new Clone2(1, "Ishan Mishra");
        Clone1 clone1 = new Clone1(2, clone2);

        final Clone1 cloned = (Clone1) clone1.clone();
        clone1.val1 = 2;
        clone2.val1 = 2;
        clone2.val2 = "Aishwarya";

        System.out.println(clone1);
        System.out.println(cloned);
    }
}

class Clone1 implements Cloneable{
    int val1;
    Clone2 val2;

    public Clone1(int val1, Clone2 val2) {
        this.val1 = val1;
        this.val2 = val2;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString() {
        return "Clone1{" +
                "val1=" + val1 +
                ", val2=" + val2 +
                '}';
    }
}

class Clone2 implements Cloneable {
    int val1;
    String val2;

    public Clone2(int val1, String val2) {
        this.val1 = val1;
        this.val2 = val2;
    }

    @Override
    public String toString() {
        return "Clone2{" +
                "val1=" + val1 +
                ", val2='" + val2 + '\'' +
                '}';
    }
}

class Address {
    String locality;
    int houseNo;
    int residentNumber;

    public Address(String locality, int houseNo, int residentNumber) {
        this.locality = locality;
        this.houseNo = houseNo;
        this.residentNumber = residentNumber;
    }

    @Override
    public String toString() {
        return "Address{" +
                "locality='" + locality + '\'' +
                ", houseNo=" + houseNo +
                ", residentNumber=" + residentNumber +
                '}';
    }
}

class Person implements Cloneable{
    String[] personName;
    Address personAddress;

    public Person(String[] personName, Address personAddress) {
        this.personName = personName;
        this.personAddress = personAddress;
    }

    @Override
    public String toString() {
        return "Person{" +
                "personName=" + Arrays.toString(personName) +
                ", personAddress=" + personAddress +
                '}';
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}