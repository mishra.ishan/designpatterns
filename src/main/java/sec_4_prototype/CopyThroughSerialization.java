package sec_4_prototype;

import org.apache.commons.lang3.SerializationUtils;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Reason for using this:-
 *
 * What if we have a deep hierarchy of objects (say 20). Then creating a copy constructor for all of them won't be possible.
 * That time, it is better to serialize the object and then deserialize it to obtain a brand new object on the heap.
 */
public class CopyThroughSerialization {
    public static void main(String[] args) {
        final Foo foo = new Foo(new String[]{"Ishan", "Mishra"}, new int[]{504, 1});
        final Foo foo2 = SerializationUtils.roundtrip(foo);

        foo2.name[0] = "Aishwarya";
        foo2.houseAndResidentNumber[1] = 2;

        System.out.println(foo);
        System.out.println(foo2);
    }
}

class Foo implements Serializable {
    String[] name;
    int[] houseAndResidentNumber;

    public Foo(String[] name, int[] houseAndLocality) {
        this.name = name;
        this.houseAndResidentNumber = houseAndLocality;
    }

    @Override
    public String toString() {
        return "Foo{" +
                "name=" + Arrays.toString(name) +
                ", houseAndResidentNumber=" + Arrays.toString(houseAndResidentNumber) +
                '}';
    }
}