package sec_4_prototype;

import java.util.Arrays;

public class UsingCopyConstructor {
    public static void main(String[] args) {
        final Employee ishan = new Employee(new String[]{"Ishan", "Mishra"}, "ishan", new Address_("S V", new int[]{504, 1}));
        final Employee aishwarya = new Employee(ishan);
        aishwarya.fullName[0] = "Aishwarya";
        aishwarya.fName = "aishwarya";
        aishwarya.address.houseAndResidentNumber[1] = 2;

        System.out.println(ishan);
        System.out.println(aishwarya);

        /**
         * String are immutable example. When uncommented, the answer won't matter as to whatever we change in the method changeStr. Strings are immutable.
         *
         * https://stackoverflow.com/questions/1270760/passing-a-string-by-reference-in-java
         */
//        String s = "ishan";
//        changeStr(s);
//        System.out.println(s);
    }

//    private static void changeStr (String s) {
//        s = s + " Mishra";
//    }
}

class Employee {
    String[] fullName;
    String fName;
    Address_ address;

    public Employee(String[] fullName, String fname, Address_ address) {
        this.fullName = fullName;
        this.address = address;
        this.fName = fname;
    }

    /**
     * Copy constructor.
     */
    public Employee(Employee other) {

        /**
         * Note that the below command will give a shallow copy. It will just create a new object (Employee), but will point to the existing objects (name[] and address) on the heap.
         */
//        this(other.name, other.address);

        /**
         * In this case, our address issue is fixed (thanks to a copy constructor in Address class), but the name is still a problem. It is happening because name[] array is still pointing to the objects on heap.
         *
         */
//        this(other.name, new Address_(other.address))

        /**
         *  Other than that, note that strings are immuatble in java. Hence, when we pass a string in a method, then we get a COPY of that (immutability).
         *
         *  Note that though strings are immutable, but arrays are not immutable.
         *  They can be altered.
         *  Hence we pass a copy of the reference to the array object on the heap.
         *  Thus, we need to provide a copy of that object instead of the actual thing.
         *
         *  See this question for better understanding:-
         *  https://stackoverflow.com/questions/15962263/making-a-deep-copy-using-a-constructor-in-java
         *
         *  Basically, it tells how in case of array of string, since arrays are mutable, but strings are not, copying an array is enough.
         *  But in case of array/list of objects, then java list is mutable, hence we need to create a copy of that, but for the underlying object, if
         *      -> underlying object is mutable: Need to provide a copy that object (along with copy of array).
         *      -> underlying object is immutable: Just provide it as it is (along with copy of array).
         */
        this(Arrays.copyOf(other.fullName, other.fullName.length), other.fName, new Address_(other.address));
    }

    @Override
    public String toString() {
        return "Employee{" +
                "fullName=" + Arrays.toString(fullName) +
                ", fName='" + fName + '\'' +
                ", address=" + address +
                '}';
    }
}

class Address_ {
    String locality;
    int[] houseAndResidentNumber;

    public Address_(String locality, int[] houseAndResidentNumber) {
        this.locality = locality;
        this.houseAndResidentNumber = houseAndResidentNumber;
    }

    public Address_ (Address_ other) {
        /**
         * Note how this won't work. It is because house and resident number is an array of integers.
         * Since arrays are not immutable, hence we would have to pass the copy of this array
         */
//        this(other.locality, other.houseAndResidentNumber);

        this (other.locality, Arrays.copyOf(other.houseAndResidentNumber, other.houseAndResidentNumber.length));
    }

    @Override
    public String toString() {
        return "Address_{" +
                "locality='" + locality + '\'' +
                ", houseAndResidentNumber=" + Arrays.toString(houseAndResidentNumber) +
                '}';
    }
}