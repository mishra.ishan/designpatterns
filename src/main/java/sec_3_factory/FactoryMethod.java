package sec_3_factory;

/**
 * Remember that we talked in motivation that as per GOF, factory is just supposed to be a static method. That's what we are doing.
 */
public class FactoryMethod {
    public static void main(String[] args) {
        final Point point = Point.newCartesianPoint(2, 3);
    }
}

class Point {
    double x, y;

    /**
     * Init using cartesian coordinates.
     */
//    Point(double a, double b) {
//        this.x = a;
//        this.y = b;
//    }

    /**
     * Init using polar coordinates
     * Not allowed to have same number of arguments in construction as above
     */
//    Point(int rho, int theta) {
//
//    }

    /**
     * UGLY
     */
//    Point (double a, double b, CoordinateSystem cs) {
//        switch (cs) {
//            case CARTESIAN:
//                this.x = a;
//                this.y = b;
//                break;
//            case POLAR:
//                this.x = a * Math.cos(b);
//                this.y = a * Math.sin(b);
//                break;
//        }
//    }

    /**
     * So, we use Factory Method.
     * How?
     * -> We use a static method INSIDE THE POINT CLASS which will help in construction.
     * -> We will make the constructors private so that the ONLY WAY TO CONSTRUCT THE OBJECT FROM OUTSIDE WILL BE A FACTORY.
     */

    public static Point newCartesianPoint (double x, double y) {
        return new Point(x, y);
    }

    public static Point newPolarCoordinate (double rho, double theta) {
        return new Point(rho * Math.cos(theta), rho * Math.sin(theta));
    }

    /**
     * Make constructor private so that only factory methods can use it.
     */
    private Point (double a, double b) {
        this.x = a;
        this.y = b;
    }
}

enum CoordinateSystem {
    CARTESIAN,
    POLAR;
}
