package sec_3_factory;

import javafx.util.Pair;
import org.reflections.Reflections;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/**
 * What is an abstract factory all about?
 * It makes a correspondence between hierarchy of objects that we have, and the hierarchy of objects that are used to construct those objects.
 * Thus it is mainly related to inheritance.
 * Abstract Factory works as a super factory. As in, it creates other Factories.
 */
public class AbstractFactory {
    public static void main(String[] args) throws Exception {
        HotDrinkMachine machine = new HotDrinkMachine();
        HotDrink tea = machine.makeDrink(HotDrinkMachine.AvailableDrink.TEA, 200);
        tea.consume();

        // interactive
        HotDrink drink = machine.makeDrink();
        drink.consume();

    }
}

interface HotDrink {
    void consume();
}

class Tea implements HotDrink {

    @Override
    public void consume() {
        System.out.println("Consuming Tea");
    }
}

class Coffee implements HotDrink {

    @Override
    public void consume() {
        System.out.println("Consuming Coffee");
    }
}

/**
 * This is our abstract factory. Note that since this factory is ABSTRACT, we cannot instantiate it.
 *
 * This ABSTRACT FACTORY WILL BE USED BY THE INHERITOR FACTORIES IN ORDER TO GET CONSTRUCTED.
 */
interface HotDrinkFactory {
    HotDrink prepare(int amount);
}

/**
 * Notice how the sub-factories have used the super factory's prepare method to get constructed
 */
class TeaFactory implements HotDrinkFactory {

    @Override
    public Tea prepare(int amount) {
        System.out.println("Preparing " + amount + " of tea");
        return new Tea();
    }
}

class CoffeeFactory implements HotDrinkFactory {

    @Override
    public Coffee prepare(int amount) {
        System.out.println("Preparing " + amount + " of coffee");
        return new Coffee();
    }
}

class HotDrinkMachine
{
    public enum AvailableDrink
    {
        COFFEE, TEA
    }

    private Dictionary<AvailableDrink, HotDrinkFactory> factories =
            new Hashtable<>();

    private List<Pair<String, HotDrinkFactory>> namedFactories =
            new ArrayList<>();

    public HotDrinkMachine() throws Exception
    {
        // option 1: use an enum
        for (AvailableDrink drink : AvailableDrink.values())
        {
            String s = drink.toString();
            String factoryName = "" + Character.toUpperCase(s.charAt(0)) + s.substring(1).toLowerCase();
            Class<?> factory = Class.forName("com.activemesa.creational.factories." + factoryName + "Factory");
            factories.put(drink, (HotDrinkFactory) factory.getDeclaredConstructor().newInstance());
        }

        // option 2: find all implementors of IHotDrinkFactory
        Set<Class<? extends HotDrinkFactory>> types =
                new Reflections("com.activemesa.creational.factories") // ""
                        .getSubTypesOf(HotDrinkFactory.class);
        for (Class<? extends HotDrinkFactory> type : types)
        {
            namedFactories.add(new Pair<>(
                    type.getSimpleName().replace("Factory", ""),
                    type.getDeclaredConstructor().newInstance()
            ));
        }
    }

    public HotDrink makeDrink() throws IOException
    {
        System.out.println("Available drinks");
        for (int index = 0; index < namedFactories.size(); ++index)
        {
            Pair<String, HotDrinkFactory> item = namedFactories.get(index);
            System.out.println("" + index + ": " + item.getKey());
        }

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while (true)
        {
            String s;
            int i, amount;
            if ((s = reader.readLine()) != null
                    && (i = Integer.parseInt(s)) >= 0
                    && i < namedFactories.size())
            {
                System.out.println("Specify amount: ");
                s = reader.readLine();
                if (s != null
                        && (amount = Integer.parseInt(s)) > 0)
                {
                    return namedFactories.get(i).getValue().prepare(amount);
                }
            }
            System.out.println("Incorrect input, try again.");
        }
    }

    public HotDrink makeDrink(AvailableDrink drink, int amount)
    {
        return factories.get(drink).prepare(amount);
    }
}
