package sec_3_factory;

/**
 * When we have a LOT OF FACTORY METHODS IN A CLASS, WE MAY WANT TO GROUP THEM TOGETHER.
 * We may want to group them together and remove them from the class they are constructing.
 * Thus, we put them into a separate class whose whole SRP is to create objects for that class.
 *
 * Now, we can obtain this in two ways:-
 *  -> 1. Separate class. (Will have to keep the 'Point' class (object being created) constructors public)
 *  -> 2. Inner static class. (Can keep the 'Point' class (object being created) constructors private)
 */
public class Factory {
    public static void main(String[] args) {
        final Point1 point1 = Point1.Factory.newCartesianPoint(2, 3);
    }
}

class Point1 {
    double x, y;

    private Point1(double x, double y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Done in order to keep the Point1 constructors private. Only way to create Point1 should be via Factory.
     *
     * **>> NOTE: If we are okay with separate object creation of Point1, then we can move this factory as a separate class.
     */
    public static class Factory {
        public static Point1 newCartesianPoint(double x, double y) {
            return new Point1(x, y);
        }
        public static Point1 newPolarCoordinatePoint (double rho, double theta) {
            return new Point1(rho * Math.cos(theta), rho * Math.sin(theta));
        }
    }
}