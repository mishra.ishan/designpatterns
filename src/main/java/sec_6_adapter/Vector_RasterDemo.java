package sec_6_adapter;

import java.util.ArrayList;
import java.util.List;

/**
 * We use the concept of vectors and rasters to demonstrate the adapter pattern. What are they?
 *      -> Both concepts are used in graphic design.
 *      -> The main difference between vector and raster graphics is that raster graphics are composed of pixels, while vector graphics are composed of paths. A raster graphic, such as a gif or jpeg, is an array of pixels of various colors, which together form an image.
 *      -> When we zoom into a vector image, it won't blur since it is composed of lines and other shapes, hence it can be recaliberated.
 *      -> Raster images are composed of pixels. If we zoom such an image, it will get blurred (jpeg).
 *
 *  In this example, we shall be given an API to draw a point. We are also given a bunch of vector objects (Vector Rectangles to be exact). We have to create an adapter to convert that image (list of vector rectangles) to raster (list of points).
 *  Thus, the adapter would have to convert a given line into list of points (not just 2 points, but all the points that it is comprised of).
 *  We should be able to draw an image in either vector form (as an array of lines), or in raster form (as an array of points).
 */
public class Vector_RasterDemo {
    static final List<VectorObject> givenListOfLines = new ArrayList<>();

    static {
        final VectorRectangle givenVectorRectangle1 = new VectorRectangle(1, 1, 10, 10);
        final VectorRectangle givenVectorRectangle2 = new VectorRectangle(3, 3, 6, 6);
        givenListOfLines.add(givenVectorRectangle1);
        givenListOfLines.add(givenVectorRectangle2);
    }

    static void givenApiToDrawPoint (Point p) {
        System.out.println(".");
    }

    static void draw() {
        /** For each of the given vector image (vector = collection of lines)*/
        givenListOfLines.forEach(vectorObject -> {
            /** For each of the individual line constituting the vector image, convert that line to list of points.*/
            vectorObject.forEach(line -> {
                final LineToPointAdapter points = new LineToPointAdapter(line);
                points.forEach(point -> {
                    givenApiToDrawPoint(point);
                });
            });
        });
    }

    public static void main(String[] args) {
        draw();
        /** Multiple time calling draw() on same set of points create new points EVERYTIME */
        draw();
    }
}

/**
 * Given the above API and the list of vector objects, we need to convert the vector object to raster object (which is a list of lines).
 * -> Thus, we create a LineToPointAdapter class.
 * -> It can be a class or an interface.
 * -> It's SRP: Provide it with a single line. It shall convert it into a List of points.
 * -> Thus, few things to notice:
 *      -> ALL ADAPTERS ARE DIFFERENT. EVERY LINE MIGHT HAVE A NEW INSTANCE OF AN ADAPTER.
 *
 */

class Point {
    int x, y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}

class Line {
    Point start, end;

    public Line(Point start, Point y) {
        this.start = start;
        this.end = y;
    }

    @Override
    public String toString() {
        return "Line{" +
                "x=" + start +
                ", y=" + end +
                '}';
    }
}

/**
 * Vector object is anything that is made up of a bunch of lines.
 */
class VectorObject extends ArrayList<Line> {

}

class VectorRectangle extends VectorObject {
    /**
     * Now, rectangle can be made up if given x, y coordinates as well as width and height.
     * Then to construct a rectangle (viz an array of lines), we need to add the list of lines making up the rectangle.
     * Now, Line itself needs 2 points to construct.
     */
    VectorRectangle(int x, int y, int width, int height) {
        add(new Line(new Point(x,y), new Point(x+width, y) ));
        add(new Line(new Point(x+width,y), new Point(x+width, y+height) ));
        add(new Line(new Point(x,y), new Point(x, y+height) ));
        add(new Line(new Point(x,y+height), new Point(x+width, y+height) ));
    }
}

/**
 * In adapter pattern lingo,
 * -> Adaptee: A class / interface that is converted to another class / interface using the adapter pattern.
 * -> There are 2 ways to use the adapter pattern:-
 *      -> Class Adapter (extend adaptee, ie line in our example)
 *      -> Object Adapter (take adaptee (i.e., line) as constructor param)
 * -> Difference between the two: https://stackoverflow.com/questions/5467005/adapter-pattern-class-adapter-vs-object-adapter
 * https://stackoverflow.com/questions/9978477/difference-between-object-adapter-pattern-and-class-adapter-pattern
 *
 * Basic difference is that of composition vs inheritance.
 * -> Prefer composition (object creation by using wrappers) over inheritance (subclassing).
 *
 * Thus, there are 2 ways to convert given line to set of points:-
 *      -> 1. Sublass the Adaptee (Line).
 *      -> 2. Compose itself of adaptee (use Line in construction of this object).
 *
 * Very good example of object adapters: https://stackoverflow.com/questions/43750359/are-we-using-the-adapter-design-pattern-everytime-we-extend-a-class-and-implemen
 *
 * We shall be using object adapter (passing adaptee (line) as constructor param). Note that we need to create new object from an existing object here, unlike the other examples where we just need to implement 1 or 2 methods of those classes. Hence we would need this adapter to give me a list of lines.
 * -> To obtain the final output, we can either use an instance variable or we can extend the ArrayList<Point> to LinetoPointAdapter.
 * -> But note that this instance (just like a real world adapter) SHOULD BE a list points.
 * -> Adapter should input the old and output the useful new.
 * -> Thus, the INSTANCE CREATED AFTER PASSING VIA OUR ADAPTER SHOULD BE AN ARRAY OF POINTS (VIZ WHAT WE REQUIRED) SO THAT WE CAN USE IT SO. IF WE DON'T EXTEND THE ARRAY OF POINTS, THEN WHAT DOES THIS ADAPTER DO? WASN'T IT SUPPOSED TO JUST GIVE ME AN ARRAY OF POINTS CORRESPONDING TO A LINE?????
 */
class LineToPointAdapter extends ArrayList<Point> {

    private static int noOfTimesConstructorCalledAndNewPointsCreated = 0;

    LineToPointAdapter(Line line) {
        System.out.println(
                String.format("%d: Generating points for line [%d,%d]-[%d,%d] (no caching)",
                        ++noOfTimesConstructorCalledAndNewPointsCreated, line.start.x, line.start.y, line.end.x, line.end.y));

        int left = Math.min(line.start.x, line.end.x);
        int right = Math.max(line.start.x, line.end.x);
        int top = Math.min(line.start.y, line.end.y);
        int bottom = Math.max(line.start.y, line.end.y);
        int dx = right - left;
        int dy = line.end.y - line.start.y;

        if (dx == 0)
        {
            for (int y = top; y <= bottom; ++y)
            {
                add(new Point(left, y));
            }
        }
        else if (dy == 0)
        {
            for (int x = left; x <= right; ++x)
            {
                add(new Point(x, top));
            }
        }
    }
}