package sec_6_adapter;

import java.util.Map;

public class AdapterCodingExercise {
}

class Square
{
    public int side;

    public Square(int side)
    {
        this.side = side;
    }
}

interface Rectangle
{
    int getWidth();
    int getHeight();

    default int getArea()
    {
        return getWidth() * getHeight();
    }
}

/**
 * Adaptee: Square
 */
class SquareToRectangleAdapter implements Rectangle
{
    int widht, height;
    public SquareToRectangleAdapter(Square square)
    {
        this.widht = square.side;
        this.height = square.side;
    }

    @Override
    public int getWidth() {
        return widht;
    }

    @Override
    public int getHeight() {
        return height;
    }
}