package sec_6_adapter;

import java.util.*;
import java.util.function.Consumer;

/**
 * Problem with before approach, we use a lot of different objects that were made.
 * What we mean by that is that if we had a line and we created set of points for it. Now say the same line came again.
 * Should we create new set of points for it? Obviously no.
 * We just have to create an adapter. If we use the new adapter for the same line, it won't be a problem at all.
 *
 * Thus, in general, adapters are responsible for creating a lot of different compatible objects.
 * We may not want to buy a new adapter everytime.
 * This is what we shall be doing here...
 */
public class AdapterCaching {
    static final List<VectorObject1> givenListOfLines = new ArrayList<>();

    static {
        final VectorRectangle1 givenVectorRectangle1 = new VectorRectangle1(1, 1, 10, 10);
        final VectorRectangle1 givenVectorRectangle2 = new VectorRectangle1(3, 3, 6, 6);
        givenListOfLines.add(givenVectorRectangle1);
        givenListOfLines.add(givenVectorRectangle2);
    }

    static void drawPoint (Point1 point1) {
        System.out.println(".");
    }

    static void draw() {
        givenListOfLines.forEach(vectorObject -> {
            vectorObject.forEach(line -> {
                final LineToPointAdapter1 points = new LineToPointAdapter1(line);
                points.forEach(AdapterCaching::drawPoint);
            });
        });
    }

    public static void main(String[] args) {
        draw();
        /** Multiple time calling draw() do not create new points */
        draw();
    }
}

/**
 * Need to implement equals and hashcode for differentiation between different points.
 */
class Point1 {
    int x, y;

    public Point1(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Point1)) return false;
        Point1 point1 = (Point1) o;
        return x == point1.x &&
                y == point1.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    @Override
    public String toString() {
        return "Point1{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}

/**
 * To implement caching, we need to differentiate between different objects. Hence override hashcode and equals.
 */
class Line1 {
    Point1 start, end;

    public Line1(Point1 start, Point1 end) {
        this.start = start;
        this.end = end;
    }

    @Override
    public String toString() {
        return "Line1{" +
                "start=" + start +
                ", end=" + end +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Line1)) return false;
        Line1 line1 = (Line1) o;
        return Objects.equals(start, line1.start) &&
                Objects.equals(end, line1.end);
    }

    @Override
    public int hashCode() {
        return Objects.hash(start, end);
    }
}

class VectorObject1 extends ArrayList<Line1> {}

class VectorRectangle1 extends VectorObject1 {
    public VectorRectangle1(int x, int y, int width, int height)
    {
        add(new Line1(new Point1(x,y), new Point1(x+width, y) ));
        add(new Line1(new Point1(x+width,y), new Point1(x+width, y+height) ));
        add(new Line1(new Point1(x,y), new Point1(x, y+height) ));
        add(new Line1(new Point1(x,y+height), new Point1(x+width, y+height) ));
    }
}

/**
 * Now, our adapter needs a line for construction and would give out a list of points.
 * But if the line was already made, we would just return the cached set of points for the same.
 * We can do it either way. Either extends the array list.
 * OR
 * (BETTER):- implement iterables.
 * Reason: Composition over inheritance. Should not extends until necessary.
 */
class LineToPointAdapter1 implements Iterable<Point1>{

    private static int noOfTimesConstructorCalledAndNewPointsCreated = 0;
    private static Map<Integer, ArrayList<Point1>> hashCodeVsIterableCache = new HashMap<>();
    /**
     * Store the hashcode of the current object. This would be useful because we would need this line to remain unique throughout.
     */
    private int hashOfCurrentLine;

    LineToPointAdapter1(Line1 line) {
        hashOfCurrentLine = line.hashCode();
        /**
         * If the line was already made and is cached, then defer from making the points a second time.
         * Just return an empty object, which has not made any points.
         */
        if(hashCodeVsIterableCache.containsKey(hashOfCurrentLine)) {
            return;
        }
        /**
         * Else, create the new points AND CACHE THE POINTS.
         */
        System.out.println(
                String.format("%d: Generating points for line [%d,%d]-[%d,%d] (with caching)",
                        ++noOfTimesConstructorCalledAndNewPointsCreated, line.start.x, line.start.y, line.end.x, line.end.y));

        ArrayList<Point1> pointList = new ArrayList<>();
        int left = Math.min(line.start.x, line.end.x);
        int right = Math.max(line.start.x, line.end.x);
        int top = Math.min(line.start.y, line.end.y);
        int bottom = Math.max(line.start.y, line.end.y);
        int dx = right - left;
        int dy = line.end.y - line.start.y;

        if (dx == 0)
        {
            for (int y = top; y <= bottom; ++y)
            {
                pointList.add(new Point1(left, y));
            }
        }
        else if (dy == 0)
        {
            for (int x = left; x <= right; ++x)
            {
                pointList.add(new Point1(x, top));
            }
        }
        hashCodeVsIterableCache.put(hashOfCurrentLine, pointList);
    }

    /**
     * Now, suppose a line L is created.
     * Whenever we shall call it for iteration, then it will come to iterator method, and it would trigger the following code.
     * It will just use the cache and send the cached set of points for iterations.
     *
     * When the same line creation is triggered again, then we will just create an empty adapter instance. We shall just use the set the points already created(the temporary objects created during the adapter instantiation) and return their iterator.
     */
    @Override
    public Iterator<Point1> iterator() {
        return hashCodeVsIterableCache.get(hashOfCurrentLine).iterator();
    }

    @Override
    public void forEach(Consumer<? super Point1> action) {
        hashCodeVsIterableCache.get(hashOfCurrentLine).forEach(action);
    }

    @Override
    public Spliterator<Point1> spliterator() {
        return hashCodeVsIterableCache.get(hashOfCurrentLine).spliterator();
    }

}