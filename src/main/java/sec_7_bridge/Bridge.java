package sec_7_bridge;

/**
 * Need for bridge:-
 * To avoid cartesian product complexity.
 * How:-
 * Use abstractions.
 * Encapsulate an implementation class (Whatever we are given) inside of an interface class (ABSTRACT CLASS) (whatever we need to be generic with different implementations).
 *
 * For this example, suppose we can have a lot of shapes:- like circle, triangle, square etc.
 * Along with that, we can have many types of rendering possible for those shapes like vector rendering, raster rendering etc.
 *
 * Now, suppose we want to create renderers (that is, a program/method which would take the objects as input and it would render those shapes in the output terminal (say monitor or printer etc)). (java.lang.Object javax.faces.render.Renderer public abstract class Renderer extends Object. A Renderer converts the internal representation of UIComponent s into the output stream (or writer) associated with the response we are creating for a particular request.) (https://www.google.com/search?q=what+is+a+renderer+in+java&oq=what+is+a+renderer&aqs=chrome.1.69i57j0l7.6436j1j7&sourceid=chrome&ie=UTF-8)
 *
 * If we were not using the bridge pattern, what would we end up with is a separate scratch implementation for all renderers. We would have a circle vector renderer (which would take vector circle object as an input and would render it to the output stream), a circle raster rendered (which would take vector circle object as an input and would render it to the output stream), and similar renderers for square and triangle and all other shapes.
 *
 * That would mean a lot of dev effort (which to be honest I totally end up doing). Thus, instead what we can use is the bridge pattern. How?
 *
 * We will put the renderer inside the shape. (ENCAPSULATE IMPLEMENTATION (RENDERER) INSIDE INTERFACE (SHAPE))
 */
public class Bridge {
    public static void main(String[] args) {
        final RasterCircleRenderer rasterCircleRenderer = new RasterCircleRenderer();
        final VectorCircleRenderer vectorCircleRenderer = new VectorCircleRenderer();

        /**
         * Note how easy it is to just swap both the renderers WITHOUT ANY CODE MODIFICATIONS.
         */

        /**
         * Note that shape here is the interface/abstract class (which we want to be generic and must be swappable with different implementations of circle renderers).
         * We are given different implementations of renderers.
         */
        final RenderableCircle vectorRenderableCircle = new RenderableCircle(vectorCircleRenderer, 10.0f);
        vectorRenderableCircle.draw();
        vectorRenderableCircle.resizeByFactor(2);
        vectorRenderableCircle.draw();

        final RenderableCircle rasterRenderableCircle = new RenderableCircle(rasterCircleRenderer, 50.0f);
        rasterRenderableCircle.draw();
        rasterRenderableCircle.resizeByFactor(2);
        rasterRenderableCircle.draw();
    }
}

/**
 * Assume only circle. Thus, without bridge pattern, we could have ended with 2 separate impls: VectorCircleRenderer and RasterCircleRenderer.
 *
 * Instead, let's use bridge pattern and let's see how it helps us.
 */

/**
 * All classes which can render a circle WILL IMPLEMENT THIS INTERFACE.
 */
interface CircleRenderer {
    void renderCircle(float radius);
}

/**
 * Render a circle composed of lines.
 */
class VectorCircleRenderer implements CircleRenderer {

    @Override
    public void renderCircle(float radius) {
        System.out.println("Rendering Circle composed of lines. Radius: " + radius);
    }
}

/**
 * Render a circle composed of pixels.
 */
class RasterCircleRenderer implements CircleRenderer {

    @Override
    public void renderCircle(float radius) {
        System.out.println("Rendering Circle composed of pixels. Radius: " + radius);
    }
}

/**
 *
 * ---------------- NOW WE APPLY THE BRIDGE PATTERN TO THE INTERFACE THAT WE ACTUALLY HAVE TO DRAW / IMPLEMENT ---------------------
 *
 * Now, we are going to use the bridge pattern to implement shapes. Basically, we will put renderer inside the shape object.
 * Thus, any renderable shape needs a renderer (to instantiate itself).
 *
 * NOTE THAT IN BRIDGE PATTERN, THERE IS ALWAYS AN INTERFACE CLASS (ABSTRACT CLASS CALLED SHAPE HERE) WHICH HAS THE IMPLEMENTATION CLASS (ANY INSTANCE OF CIRCLE RENDERER HERE) ENCAPSULATED IN IT (VIA COMPOSITION (THAT IS, PASS THE OBJECT FOR CONSTRUCTION)).
 * NOTE THAT THE IMPLEMENTATION THAT WE PASS (IN CONSTRUCTOR) WILL CREATE THAT INSTANCE. For example, in our case, if we pass a Vector Renderer, we will get a vector circle renderer. If we pass a Raster Renderer, we will get a Raster Circle Renderer.
 */

/**
 * Any renderable shape could be drawn or could be resized. Note that to draw, we need an instance of CircleRenderer.
 */
abstract class RenderableShape {

    protected CircleRenderer circleRenderer;

    /**
     * Note that this is important. All the implementations that we want to bridge need to go into the interface's constructor so that the classes which implement this interface can just pass the required renderer and get the required object.
     */
    public RenderableShape(CircleRenderer circleRenderer) {
        this.circleRenderer = circleRenderer;
    }
    public abstract void draw();
    public abstract void resizeByFactor(float factor);
}

class RenderableCircle extends RenderableShape {

    private float radius;

    /**
     * Note that whenever we create this Renderable circle, we need to pass how we want this circle to be rendered.
     * We can Pass either VectorRenderer or RasterRenderer.
     */
    public RenderableCircle(CircleRenderer circleRenderer) {
        super(circleRenderer);
    }

    public RenderableCircle(CircleRenderer circleRenderer, float radius) {
        super(circleRenderer);
        this.radius = radius;
    }

    @Override
    public void draw() {
        circleRenderer.renderCircle(radius);
    }

    @Override
    public void resizeByFactor(float factor) {
        radius *= factor;
    }
}