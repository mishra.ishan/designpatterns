package sec_7_bridge;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;

/**
 * Problem in previous approach, everytime we need to create our interface, we need a new instance of our implementation.
 * It is okay in a small application like this. But, if the application size increases, we would have a problem to create a new instance everytime.
 * Mostly, we would want the same configured implementation in each of our interfaces.
 * That is, we want to call the same constructor everytime. Hence, instead of calling the same constructor everytime using new(), how about we use a DI framework?
 * We do exactly that here.
 */
public class BridgeWithDI {
    public static void main(String[] args) {
        final Injector injector = Guice.createInjector(new ShapeModule());
        /**
         * Now, to create a circle, we will use DI.
         * We already have the injector configured to use RenderableCircle constructor and to inject a VectorCircleRenderer automatically to it.
         * THUS, WE DIDN'T HAVE TO CALL NEW ON ALL.
         * WE JUST INJECTED.
         */

        final _RenderableCircle instance = injector.getInstance(_RenderableCircle.class);
        instance.setRadius(5f);
        instance.draw();
        instance.resizeFactor(2);
        instance.draw();
    }
}

interface _CircleRenderer {
    void render (float radius);
}

class _VectorCircleRenderer implements _CircleRenderer {

    @Override
    public void render(float radius) {
        System.out.println("Rendering Circle composed of lines. Radius: " + radius);
    }
}

class _RasterCircleRenderer implements _CircleRenderer {

    @Override
    public void render(float radius) {
        System.out.println("Rendering Circle composed of pixels. Radius: " + radius);
    }
}

abstract class _RenderableShape {

    protected _CircleRenderer circleRenderer;

    public _RenderableShape(_CircleRenderer circleRenderer) {
        this.circleRenderer = circleRenderer;
    }

    abstract void draw();
    abstract void resizeFactor(float factor);
}

class _RenderableCircle extends _RenderableShape {

    private float radius;

    /**
     * To create a DI framework, first of all, we need a constructor which is to be injected.
     * IT IS TRUE FOR ALL DI FRAMEWORKS.
     * Hence, we choose this constructor.
     * For purpose of injection, we need to chose one constructor.
     * Then, we annotate that constructor with the @Inject annotation.
     * Thus, this annotation tells the framework as to what constructor does it has to use for this class if some client wants to make a circle using injected values.
     */
    @Inject
    public _RenderableCircle(_CircleRenderer circleRenderer) {
        super(circleRenderer);
    }

    public _RenderableCircle(_CircleRenderer circleRenderer, float radius) {
        super(circleRenderer);
        this.radius = radius;
    }

    @Override
    void draw() {
        circleRenderer.render(radius);
    }

    @Override
    void resizeFactor(float factor) {
        radius *= factor;
    }

    public float getRadius() {
        return radius;
    }

    public void setRadius(float radius) {
        this.radius = radius;
    }
}

/**
 * Now, as per DI, we need to create a module WHICH CONFIGURES AS TO WHAT ACTUALLY HAPPENS WHEN SOMEBODY HAS A DEPENDENCY ON A RENDERER (OUR CASE).
 */
class ShapeModule extends AbstractModule {

    /**
     * We will tell to bind the Renderer class.
     * What this actually means is that whenever somebody actually requests a renderer to be injected, we're going to make a NEW instance of the vector renderer.
     */
    @Override
    protected void configure() {
        bind(_CircleRenderer.class).to(_VectorCircleRenderer.class);
    }
}