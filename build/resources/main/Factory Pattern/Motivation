
-> Building an object via constructor call can be too convoluted at times.
-> Constructor is not descriptive. Problems with constructor:-
    -> Name of the constructor is the name of the containing type.
        Thus, a user building the object has no idea as to what all arguments might be required to build.
        For example, if we have to construct a point, then with one constructor with x and y, we wouldn't know if x and y are cartesian coordinates or polar coordinates.
    -> Cannot overload with same sets of arguments with different names. Taking above example only, we need some sort of constructor overloading with same sets of arguments (which is not possible in java).
    -> This turns into overloading hell: Lots and lots of constructors and us having to remember their ordering and all.
-> Just an FYI: We are still concerned with wholesale creation of objects. We are not talking about piecewise creation.
-> Thus, for this wholesale object creation, we can use:-
    -> A separate function (Factory method):-
        -> A static method which returns an object. The method can have any name and any number of arguments.
        -> This static method CAN BE CONTAINED IN A SEPARATE CLASS (Factory).
        -> Factory doesn't get mentioned in GOF as such.
        -> When we have construction logic for several different kinds of classes or 1 class with different arguments, we might want to do separation of concerns in that as well and put in into a separate Factory class.
        -> In order to create a hierarchy of objects, and we have a corresponding hierarchy of factories related to that, then we can use Abstract Factory.